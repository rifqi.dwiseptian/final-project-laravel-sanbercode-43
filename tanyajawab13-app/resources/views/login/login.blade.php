@extends('layouts.auth')

@section('container')
<main class="form-signin">
    {{-- <img class="mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> --}}
    <h1 class="text-center display-6 title">Login Form</h1>

    <form action="/login" method="POST">
        @csrf
        <div class="form-floating">
            <input type="email" class="form-control b-top @error('email') is-invalid @enderror" name="email" id="email" placeholder=" " required>
            <label for="email">Email address</label>

            @error('email')
            <div class="invalid-feedback mb-1">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="form-floating">
            <input type="password" class="form-control b-bot @error('password') is-invalid @enderror" name="password" id="password" placeholder=" " required>
            <label for="password">Password</label>

            @error('password')
            <div class="invalid-feedback mb-1">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="checkbox my-2">
            <label>
                <input type="checkbox" id="showPassword"><small class="align-top">&nbsp; show password</small>
            </label>
        </div>

        <button class="w-100 btn btn-primary my-1" type="submit">Sign In</button>
    </form>

    <div class="mt-1 mb-3" style="font-size: 0.8rem">
        Not registered? <a href="/register">Register Now!</a>
    </div>
    </form>
</main>
@endsection
