@extends('layouts.master')
@section('title')
    Profile
@endsection

@section('sub-title')
    {{ $user->name }}
@endsection

@section('content')
    <form action="/profile/{{ $profile->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label class='font-weight-normal'>Biodata</label>
            <input type="text" name="biodata" value="{{ $profile->biodata }}" class="form-control">
        </div>
        @error('biodata')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label class='font-weight-normal'>Email</label>
            <input type="text" name="email" value="{{ $profile->email }}" class="form-control">
        </div>
        @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label class='font-weight-normal'>Alamat</label>
            <input type="text" name="alamat" value="{{ $profile->alamat }}" class="form-control">
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label class='font-weight-normal'>Umur</label>
            <input type="text" name="umur" value="{{ $profile->umur }}" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">submit</button>
    </form>
@endsection
