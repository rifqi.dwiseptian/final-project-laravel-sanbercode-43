<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Category;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = Question::all();
        return view('question.index', ['question' => $question]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('question.tambah', ['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'category_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048',
        ]);
        // konversi nama gambar
        $imageName = time() . '.' . $request->image->extension();

        // file gambar ke folder public/image
        $request->image->move(public_path('image'), $imageName);

        //insert ke database
        $question = new question;

        $question->user_id = $request->user_id;
        $question->description = $request->description;
        $question->category_id = $request->category_id;
        $question->image = $imageName;

        $question->save();

        return redirect('/question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);
        $answerCount = Answer::where('question_id', $id)->count();
        $answers = Answer::where('question_id', $id)->get();

        return view('question.detail', [
            'question' => $question,
            'answerCount' => $answerCount,
            'answers' => $answers
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $question = Question::find($id);
        return view('question.edit', ['question' => $question, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required',
            'category_id' => 'required',
            'image' => 'mimes:png,jpg,jpeg|max:2048',
        ]);

        $question = Question::find($id);

        $question->user_id = $request->user_id;
        $question->description = $request->description;
        $question->category_id = $request->category_id;

        if ($request->has('image')) {
            $path = 'image/';
            File::delete($path . $question->image);

            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('image'), $imageName);

            $question->image = $imageName;
        }


        $question->save();
        return redirect('/question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $path = 'image/';
        File::delete($path . $question->image);


        $question->delete();

        return redirect('/question');
    }
}
