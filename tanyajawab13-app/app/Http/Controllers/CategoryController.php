<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = DB::table('category')->get();

        return view('category.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('category.tambah');
    }

    public function store(request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => '',
        ]);

        DB::table('category')->insert([
            'name' => $request ['name'],
            'description' => $request ['description']
        ]);

        return redirect('/category');
    }


    public function show($id)
    {
        $category = DB::table('category')->find($id);

        return view('category.detail', ['category' => $category]);
    }

    public function edit($id)
    {
        $category = DB::table('category')->find($id);

        return view('category.edit', ['category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        DB::table('category')
              ->where('id', $id)
              ->update([
                'name' => $request['name'],
                'description' => $request['description']
              ]);
        
        return redirect('/category');
    }

    public function destroy($id)
    {
        DB::table('category')->where('id', '=', $id)->delete();

        return redirect('/category');
    }
    

}
