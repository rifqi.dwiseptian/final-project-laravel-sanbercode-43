<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login & Register
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'sign_in']);
Route::post('/logout', [AuthController::class, 'logout']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'sign_up']);


Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [IndexController::class, 'index']);
    Route::get('/dashboard', [IndexController::class, 'dashboard']);
    Route::get('/profile/{id}', [IndexController::class, 'profile']);
    Route::put('/profile/{id}', [IndexController::class, 'update']);

    //CRUD Category
    //Create Data
    Route::get('/category/create', [CategoryController::class, 'create']);
    Route::post('/category', [CategoryController::class, 'store']);
    //Read Data
    Route::get('/category', [CategoryController::class, 'index']);
    Route::get('/category/{id}', [CategoryController::class, 'show']);
    //Update Data
    Route::get('/category/{id}/edit', [CategoryController::class, 'edit']);
    Route::put('/category/{id}', [CategoryController::class, 'update']);
    //Delete Data
    Route::delete('/category/{id}', [CategoryController::class, 'destroy']);


    //CRUD Tanya
    Route::resource('question', QuestionController::class);

    //CRUD Jawab
    Route::resource('answer', AnswerController::class);
    
});
