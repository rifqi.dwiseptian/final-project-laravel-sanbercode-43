$("#showPassword").click(function() {
    // password
    let pass = document.getElementById('password');
    if (pass.type == 'password') {
        pass.type = 'text';
    } else {
        pass.type = 'password';
    }

    // password confirmation
    pass = document.getElementById('password_confirmation');
    if (pass) {
        if (pass.type == 'password') {
            pass.type = 'text';
        } else {
            pass.type = 'password';
        }
    }
});