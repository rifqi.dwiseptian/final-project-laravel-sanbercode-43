@extends('layouts.master')
@section('sub-title')
    <span>
        Category
    </span>
@endsection
@section('content')

    <div class="float-right">
        <a href="/category/create" class="btn btn-sm btn-light">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
             <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
        </svg>
        New Category
    </a>
    </div>

    @if ($categories->isNotEmpty())
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $key => $item)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $item->name }} </td>
                        <td>
                            <form action="/category/{{ $item->id }}" method="POST">
                                <a href="/category/{{ $item->id }}" class="btn btn-outline-info btn-sm">Detail</a>
                                <a href="/category/{{ $item->id }}/edit" class="btn btn-outline-warning btn-sm">Edit</a>
                                @csrf
                                @method('delete')
                                <input type="submit" value="Delete" class="btn btn-outline-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection
