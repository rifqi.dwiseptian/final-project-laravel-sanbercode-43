@extends('layouts.master')
@section('title')
    Create Question
@endsection
@section('sub-title')
    Question
@endsection
@section('content')

<form action="/question" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="category">Category</label>
      <select name="category_id" class="form-control">
        <option value="">--pilih kategori--</option>
          @forelse($category as $item)
              <option value="{{ $item->id }}">{{ $item->name }}</option>
          @empty
              <option value="">Belum ada kategori</option>
          @endforelse
      </select>
    </div>
    @error('category_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    
    <div class="form-group">
      {{-- sementara karena belum auth --}}
      <input type="hidden" name="user_id" value="{{ Auth::id() }}">
    </div>
    @error('user_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Question</label>
      <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Image</label>
      <input type="file" name="image" class="form-control">
    </div>
    @error('image')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">submit</button>
  </form>
@endsection