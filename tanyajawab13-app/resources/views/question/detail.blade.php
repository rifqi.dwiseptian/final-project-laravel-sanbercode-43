@extends('layouts.master')
@section('sub-title')
    <div class="small">
        <span class="text-muted">category</span>&nbsp; {{ strtolower($question->category->name) }} &emsp;
        <span class="text-muted">asked</span>&nbsp; {{ $question->created_at->format('d/m/Y') }} &emsp;
        <span class="text-muted">views</span>&nbsp; 10k times
    </div>
@endsection

@section('title')
    Question
@endsection


@section('content')
    <div class="row pl-4">

        <div class="w-100">
            {{-- edit delete --}}
            @if (Auth::id() == $question->user_id)
                <form action="{{ $question->id }}" method="post" class="mb-3">
                    <a href="{{ $question->id }}/edit" class="btn btn-sm btn-light">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                        </svg>
                        Edit
                    </a>
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-sm btn-light">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                            <path d="M14 3a.702.702 0 0 1-.037.225l-1.684 10.104A2 2 0 0 1 10.305 15H5.694a2 2 0 0 1-1.973-1.671L2.037 3.225A.703.703 0 0 1 2 3c0-1.105 2.686-2 6-2s6 .895 6 2zM3.215 4.207l1.493 8.957a1 1 0 0 0 .986.836h4.612a1 1 0 0 0 .986-.836l1.493-8.957C11.69 4.689 9.954 5 8 5c-1.954 0-3.69-.311-4.785-.793z" />
                        </svg>
                        Delete
                    </button>
                </form>
            @endif
        </div>

        {{-- Question Detail --}}
        <div class="col-6">
            <div class="mb-3">
                <b class="h4">{{ $question->description }}</b>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{ asset('/image/' . $question->image) }}" alt="">
                <div class="card-body text-right small text-secondary">
                    Asked by: {{ $question->user->name }}
                </div>
            </div>

            {{-- Answer Detail --}}
            <div>
                <div class="text-right">
                    <button type="button" class="btn btn-sm btn-light" data-toggle="modal" data-target="#createModal">
                        Add a comment
                    </button>
                </div>

                <h5>{{ $answerCount }} Answer</h5>

                @foreach ($answers as $answer)
                    <div class="shadow p-3 mb-5 bg-white rounded" data-aos="fade-up">
                        {{-- description --}}
                        {{ $answer->description }}

                        {{-- answered by --}}
                        <div class="text-right text-muted small">
                            {{ $answer->created_at->format('M d, Y') }} <br>
                            by {{ $answer->user->name }}
                        </div>

                        {{-- Comment Edit, Delete --}}
                        @if ($answer->user_id == Auth::id())
                            {{-- Edit Button --}}
                            <button id="edit" class="btn btn-sm btn-outline-warning" data-toggle="modal" data-target="#editModal{{ $loop->index }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                </svg>
                            </button>
                            &nbsp;

                            {{-- Delete Button --}}
                            <button id="delete" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#deleteModal{{ $loop->index }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                    <path d="M14 3a.702.702 0 0 1-.037.225l-1.684 10.104A2 2 0 0 1 10.305 15H5.694a2 2 0 0 1-1.973-1.671L2.037 3.225A.703.703 0 0 1 2 3c0-1.105 2.686-2 6-2s6 .895 6 2zM3.215 4.207l1.493 8.957a1 1 0 0 0 .986.836h4.612a1 1 0 0 0 .986-.836l1.493-8.957C11.69 4.689 9.954 5 8 5c-1.954 0-3.69-.311-4.785-.793z" />
                                </svg>
                            </button>
                        @endif

                    </div>
                @endforeach
            </div>




            <!-- Create Modal -->
            <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createModalLabel">Your Answer...</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/answer" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                <input type="hidden" name="question_id" value="{{ $question->id }}">
                                <div class="form-group">
                                    <textarea class="form-control" name="description" rows="5"></textarea>
                                </div>
                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Edit, Delete Modal --}}
            @foreach ($answers as $answer)
                @if ($answer->user_id == Auth::id())
                    <!-- Edit Modal -->
                    <div class="modal fade" id="editModal{{ $loop->index }}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="editModalLabel">Update Answer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/answer/{{ $answer->id }}" method="POST">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <textarea class="form-control" name="description" rows="5">{{ $answer->description }}</textarea>
                                        </div>
                                        @error('description')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <button type="submit" class="btn btn-warning float-right">Update</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Delete Modal -->
                    <div class="modal fade" id="deleteModal{{ $loop->index }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteModalLabel">Are you sure?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/answer/{{ $answer->id }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger float-right">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

        </div>
        @endforeach





    </div>

    </div>
@endsection

@push('scripts')
    <script>
        AOS.init();

        $(document).ready(function() {
            $("#edit").click(function() {});

            $("#delete").click(function() {});
        });
    </script>
@endpush
