@extends('layouts.auth')

@section('container')
    <main class="form-signin">
        {{-- <img class="mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> --}}
        <h1 class="text-center display-6 pb-1" style="font-size: 1.9rem">Register Form</h1>

        <form action="/register" method="POST">
            @csrf
            <div class="form-floating">
                <input type="text" class="form-control b-top @error('name') is-invalid @enderror" name="name"
                    id="name" placeholder=" ">
                <label for="name">Name</label>
                @error('name')
                    <div class="invalid-feedback mb-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-floating">
                <input type="email" class="form-control b-mid @error('email') is-invalid @enderror" name="email"
                    id="email" placeholder=" ">
                <label for="email">Email address</label>
                @error('email')
                    <div class="invalid-feedback mb-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-floating">
                <input type="password" class="form-control b-mid @error('password') is-invalid @enderror" name="password"
                    id="password" placeholder=" ">
                <label for="password">Password</label>
                @error('password')
                    <div class="invalid-feedback mb-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-floating">
                <input type="password" class="form-control b-bot @error('password_confirmation') is-invalid @enderror"
                    name="password_confirmation" id="password_confirmation" placeholder=" ">
                <label for="password_confirmation">Confirm Password</label>
                @error('password_confirmation')
                    <div class="invalid-feedback mb-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="checkbox my-2">
                <label>
                    <input type="checkbox" id="showPassword"><small class="align-top">&nbsp; show password</small>
                </label>
            </div>

            <button class="w-100 btn btn-danger" type="submit">Sign Up</button>
        </form>

        <div class="mt-2 mb-3" style="font-size: 0.8rem">
            Back to <a href="/login">Login Page!</a>
        </div>

    </main>
@endsection
