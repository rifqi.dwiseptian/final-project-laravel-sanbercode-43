
<?php
// Get current memory usage
$currentMemoryUsage = memory_get_usage();

// Get peak memory usage
$peakMemoryUsage = memory_get_peak_usage();

// Run garbage collection
gc_collect_cycles();

// Print out memory usage statistics
echo "Current memory usage: " . $currentMemoryUsage . " bytes\n";
echo "Peak memory usage: " . $peakMemoryUsage . " bytes\n";
echo "Memory usage after garbage collection: " . memory_get_usage() . " bytes\n";

?>