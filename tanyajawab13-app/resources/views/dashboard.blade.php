@extends('layouts.master')
@section('title')
    Dashboard
@endsection

@section('sub-title')
All Data
@endsection

@section('content')
    <div class="row">
        <div class="col-3">
            <div class="gradient rounded text-center text-white p-3">
                <div class="display-3">
                    {{ $user }}
                </div>
                <div class="lead">
                    USER
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="gradient2 rounded text-center text-white p-3">
                <div class="display-3">
                    {{ $category }}
                </div>
                <div class="lead">
                    CATEGORY
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="gradient3 rounded text-center text-white p-3">
                <div class="display-3">
                    {{ $question }}
                </div>
                <div class="lead">
                    QUESTION
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="gradient4 rounded text-center text-white p-3">
                <div class="display-3">
                    {{ $answer }}
                </div>
                <div class="lead">
                    ANSWER
                </div>
            </div>
        </div>
    </div>
@endsection
