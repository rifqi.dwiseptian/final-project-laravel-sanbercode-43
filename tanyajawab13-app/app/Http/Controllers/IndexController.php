<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        return redirect('/category');
    }

    public function profile($id)
    {

        $profile_check = DB::table('profiles')->where('user_id', $id)->count();

        $user = DB::table('users')->where('id', $id)->first();

        if ($profile_check == 0) {
            DB::table('profiles')->insert([
                'biodata' => '',
                'alamat' => '',
                'email' => $user->email,
                'umur' => 0,
                'user_id' => $id
            ]);
        }

        $profile = DB::table('profiles')->where('user_id', $id)->first();

        return view('profile', [
            'profile' => $profile,
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required'
        ]);

        DB::table('profiles')
            ->where('id', $id)
            ->update([
                'biodata' => $request['biodata'],
                'email' => $request['email'],
                'alamat' => $request['alamat'],
                'umur' => $request['umur']
            ]);
        
            $temp = Auth::id();

        return redirect('/profile/'.$temp);
    }

    public function dashboard(){
        $user = DB::table('users')->count();
        $category = DB::table('category')->count();
        $question = DB::table('questions')->count();
        $answer = DB::table('answers')->count();


        return view('/dashboard',[
            'user' => $user,
            'category' => $category,
            'question' => $question,
            'answer' => $answer 
        ]);
    }
}
