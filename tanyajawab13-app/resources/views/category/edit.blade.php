@extends('layouts.master')
@section('title')
    Edit Category
@endsection
@section('content')
    <form action="/category/{{ $category->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Category Name</label>
            <input type="text" name="name" value="{{ $category->name }}" class="form-control">
        </div>
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Category Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10">{{ $category->description }}</textarea>
        </div>
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">submit</button>
    </form>
@endsection
