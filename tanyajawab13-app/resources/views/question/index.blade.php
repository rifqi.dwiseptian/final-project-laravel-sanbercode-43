@extends('layouts.master')

@section('title')
    Question List
@endsection

@section('sub-title')
    <a href="/question/create" class="btn btn-sm btn-light">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
             <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
        </svg>
        New Question
    </a>
@endsection


@section('content')
    <div class="row">
        @forelse ($question as $item)
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('/image/' . $item->image) }}" height='200px'alt="">
                    <div class="card-body">
                        <p class="card-text lead">{{ Str::limit($item->description, 30) }}</p>
                        <a href="question/{{ $item->id }}" class="btn btn-sm btn-outline-info float-right">Read More...</a>
                    </div>
                </div>
            </div>
        @empty
            <h1>Tidak ada pertanyaan</h1>
        @endforelse
    </div>
@endsection
